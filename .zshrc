#!/usr/bin/env bash

# CHANGE
DISTRIBUTION=""

# EDITOR
export EDITOR=gedit

# DO NOT CHANGE
XDG_CONFIG_HOME="$HOME/.config"
source $XDG_CONFIG_HOME/zsh/global
source $XDG_CONFIG_HOME/zsh/distribution/$DISTRIBUTION

