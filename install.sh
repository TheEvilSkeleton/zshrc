#!/usr/bin/env bash
# Installation script for TheMainOne/zshrc

# Create RNG.
RNG='.zsh_bak_'$((1 + RANDOM % 69420666)) && mkdir -p ~/$RNG
echo "Created $RNG"

# Move history into the Random Number
mv "$HOME/.config/zsh/history/history" "$HOME/$RNG"

# Delete old ZSH config, if it exists
[ -d "$HOME/.config/zsh/" ] && rm -rf "$HOME/.config/zsh/"

cp -r ./.zshrc "$HOME/.zshrc"
cp -r ./.config/zsh "$HOME/.config/zsh"

mv "$HOME/$RNG/history" "$HOME/.config/zsh/history/"

# Add ':?'. Just in case $RNG is empty.
rm -rf "$HOME/${RNG:?}"

# FINISHING UP
# Warn user, the red color will bring even more attention
echo -e "\e[0;31mMUST CHANGE LINE 4 OF `~/.zshrc` TO ONE OF THE BELOW:\e[0m
=======================================
fedora
gentoo
nixos
tux # ONLY INSTALL FOR ROOT
======================================="
