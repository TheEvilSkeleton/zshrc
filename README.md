# zshrc

Personal zsh configurations.

## Dependencies

- `lsd`: replacement of `ls`
- `bat`: replacement of `cat`
- `neofetch`

## Installation

In the repository, run:

```bash
./install.sh
```